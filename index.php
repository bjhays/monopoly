<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Monopoly @ home</title>
<script type="text/javascript" src="./js/jquery.js">//version 1.7.1</script> 
<script type="text/javascript" src="./js/monopoly.js"></script>
<link rel="stylesheet" href="./css/main.css" type="text/css" media="all">
</head>

<body>
<div id="wrapper">
    <div id="title">
   	 	<h1 class="shadow">Monopoly Bank</h1>
    </div><!--title-->
    <div id="left" class="left half relative">
    	
    	<div id="theBank" class="bordNtrans shadow">
    <h2 class="shadow">The Bank</h2>
    <table id="bankTable" align="center">
      <tr>
        <th> From (-) </th>
        <th> To (+) </th>
      </tr>
      <tr>
        <td id="tdPlayerMinus"><label>
          <select name="playerMinus" id="playerMinus">
            <option value="0">Bank</option>
          </select>
        </label></td>
        <td id="tdPlayerPlus"><label>
          <select name="playerPlus" id="playerPlus">
            <option value="0">Bank</option>
          </select>
        </label></td>
      </tr>
      <tr>
        <td colspan="2" id="tdPlayerMinus"><label>
          $
          <input type="text" name="amount" id="amount" />
        </label>      <input id="goBtn" name="goBtn" type="submit" value="Go" onclick="updatePlayerCash();" /></td>
        </tr>
     </table>
    </div><!--theBank-->
    </div><!--left-->
	<div id="right" class="right half relative">
		<div id="playersDiv" class="bordNtrans shadow">
            <h2 class="shadow">The Players</h2>       
            <table id="playersTable" align="center">
                <thead><tr>
                  <th id="name">Player </th>
                  <th id="total">Total</th>
                </tr></thead>
                <tbody></tbody>
            </table>
		</div><!--playersDiv-->
	</div><!--right-->
	<div class="clear"></div>
	<div id="gameOptions">
        <label>
            <input id="new" name="new" type="button" value="New Game"/>
        </label>
        <label>
            <input id="reset" name="reset" type="button" value="Reset Game"/>
        </label>
        <label>
        	<input type="submit" name="saveGame" id="saveGame" value="Save Game" />
        </label>
        <div class="bordNtrans">
        	<label>
            	<input name="autoSave" id="autoSave" type="checkbox" value="1" checked /> Autosave?
            </label>
        </div>
    </div><!--gameOptions-->
    <div id="msg"></div><!--msg-->
</div><!--wrapper-->
</body>
</html>

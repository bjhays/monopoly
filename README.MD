Do you ever get tired of dealing with all the money in monopoly? I did! I was given Electronic Monopoly 
to make things easier, but the "banker" contraption made game play slow (loading the cards and waiting 
for the thing to register the "credit cards"). So during one game with family I wrote this small program
to do the banking instead.

Requirements
-------------------------------------------------------------------
	PHP 5+
	mySQL 4+
	Apache or IIS



Install to save games (optional)
-------------------------------------------------------------------
	1. Extract all files to a 'monopoly' folder on XAMPP or other LAMP-like server under htdocs
	2. Create user "muser" with password 'mpass' on mysql
	3. Install database structure in 'db.sql' found in the "install" directory.
	
Use
-------------------------------------------------------------------
	1. Open your favorite browser
	2. Go to http://localhost/monopoly/
	3. Get your Monopoly board game and some friends
	4. Give the laptop to the banker and get your game on!


<?php 
class mysql {

	private $hostname="";
	private $uname="";
	private $dbname="";
	private $upass=""; 
	public $dbi="";
	
	function __construct($uname, $upass, $dbname, $host="localhost") {
		$this->hostname=$host;
		$this->uname=$uname;
		$this->upass=$upass;
		$this->dbname=$dbname;
		$this->connect();
	}
	
	public function connect(){
		$this->dbi = mysql_connect($this->hostname, $this->uname, $this->upass)
		or die('Could not connect: ' . mysql_error());
		mysql_select_db($this->dbname) or die('Could not select database '.$this->dbname);
	}
	
	public function query($qryStr){
		$ret = mysql_query($qryStr) 
				or die('Query failed: ' . mysql_error() . "<br> $qryStr");
		return $ret;
	}

	public function disconnect(){
		mysql_close($this->dbi);
	}
	
	function __destruct() {
		$this->disconnect();
	}

} 
?>
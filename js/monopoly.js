// JavaScript Document
var ajaxurl = "ajax.php";
var numPlayers=0;
var playersArr=new Array();
var autoSave=true;

jQuery(document).ready(function($) {
	//when page has loaded
	//Load game or new game?
	
	
	setTimeout("autoSaveGame",60*60*1000); //Autosave every hour
	
	var oldGame=confirm("OK to load saved game? Cancel to start new");
	if (oldGame==true){
		//Load the old game from DB
		var data = {action: 'load_game'};
				
		jQuery.post(ajaxurl, data, function(response) {
			 jsonRet=jQuery.parseJSON( response );
			 jQuery.each(jsonRet, function(index, curPlayer) {
				if(index!="msg"){
					addPlayer( curPlayer.name, curPlayer.id, curPlayer.money);
					playersArr[curPlayer.id]=curPlayer.name;
					numPlayers++;
				}
			 });// end each
			 $('#msg').html(jsonRet.msg);
		});//end post
	}//end if oldGame
	else{
		//Setup new game
		//1. ask for # of players
		numPlayers=prompt("Enter Number of Players","2");
		
		if (numPlayers!=null && numPlayers!=""){
			//2. then ask for names
			
			for(i=1;i<=numPlayers;i++){
				playersArr[i]=prompt("Enter player "+i+"'s name","Harry Potter");
			}//end For
			/*
			3. build table based on input
				Default $ is 1500
			*/
			$.each(playersArr,function(index) { 
				if(index!=0){
					addPlayer(playersArr[index],index,"1500");
				}
			});//end each player
		}//end if
		}//end if new or old
		
		$('#autoSave').click(function() {
			if(autoSave){ autoSave=false; }
			else{ autoSave=true; }
		});
		
		$('#new').click(function() {
			newGame();
		});
		
		$('#reset').click(function() {
			resetGame();
		});
		
		$('#saveGame').click(function() {
			saveGame();
		});
		
}); //end doc ready

function resetGame(){
	var resetGame=confirm("OK to reset game to start ($1500 each)?");
	if(resetGame){
		for(i=0;i<numPlayers;i++){
			$('#player'+i+'Cash').val("1500");
		}//end For
	}
}

function addPlayer(pName,pIndex,pCash){
	var tblRow="<td>"+pName+"</td><td class='totals'>$"+createInput("player"+pIndex+"Cash", pCash)+"</td>";
				
	$("#playersTable tbody").append("<tr id='player"+pIndex+"'>"+tblRow+"</tr>");
	$('#playerMinus').append($('<option>', { value : pIndex }).text(pName)); 
	$('#playerPlus').append($('<option>', { value : pIndex }).text(pName)); 
}

function createInput(id, defaultVal){
	return '<input id="'+id+'" name="'+id+'" class="players" type="text" value="'+defaultVal+'" />';
}

function updatePlayerCash(){
	var transAmount=parseInt($('#amount').val());
	
	var pPlusId=$('#playerPlus').val();
	var pMinusId=$('#playerMinus').val();
	
	if(!isNaN(transAmount) && transAmount>0 && (pPlusId!=0 || pMinusId!=0) && pPlusId!=pMinusId){
	
		var pPlusVal=parseInt($('#player'+pPlusId+'Cash').val());
		var pMinusVal=parseInt($('#player'+pMinusId+'Cash').val());
		
		if(pPlusId>0){
			$('#player'+pPlusId+'Cash').val(pPlusVal+transAmount);
		}
		if(pMinusId>0){
			$('#player'+pMinusId+'Cash').val(pMinusVal-transAmount);
		}
		
		$("#msg").html("Cha Ching!");
	}else if(pPlusId==pMinusId){
		$("#msg").html("Please select two players or a player and the bank.");
	}else{
		$("#msg").html("At least one player must be selected and the amount must be larger than 0.");
	}//end else	
}

function autoSaveGame(){
	if(autoSave){
		setTimeout("autoSaveGame",60*60*1000); //Autosave every hour
		saveGame;
	}
}

function saveGame(){
	var data = {action: 'save_game'};
	
	$.each(playersArr,function(i) { 
		var cash = $('#player'+i+'Cash').val();
		data[i]=playersArr[i]+"~"+cash;
	});//end each player
	console.log("Data: %o",data);	
			
	jQuery.post(ajaxurl, data, function(response) {
		 jsonRet=jQuery.parseJSON( response );
		 $("#msg").html(jsonRet.msg);
	});//end post
}

function newGame(){
	location.reload(true);
}